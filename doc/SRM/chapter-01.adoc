[[introduction]]
== Introduction

This document is the *System Reference Manual* for BeagleBone AI-64
and covers its use and design. The board will primarily be referred to
in the remainder of this document simply as the board, although it may
also be referred to as AI-64 or BeagleBone AI-64 as a reminder.

This design is subject to change without notice as we will work to keep
improving the design as the product matures based on feedback and
experience. Software updates will be frequent and will be independent of
the hardware revisions and as such not result in a change in the
revision number.

Make sure you frequently check the 
https://git.beagleboard.org/beagleboard/beaglebone-ai-64/[BeagleBone AI-64 git repository]
for the most up to date support documents.

